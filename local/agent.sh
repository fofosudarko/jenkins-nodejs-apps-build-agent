#!/bin/bash
#
# File: agent.sh -> builds and deploys Nodejs apps on local Jenkins server
#
# Author: Frederick Ofosu-Darko <frederick.ofosu-darko@minex360.com, fofosudarko@gmail.com>
#
# Usage: bash agent.sh <LOCAL_PROJECT> <APP_ENTRY_POINT> <APP_PORT> <APP_ENVIRONMENT>
#
#

## - start here

# jenkins pulls node repository to a workspace
# copy workspace to a defacto nodejs apps directory
# change to that directory
# run npm install to install dependencies
# run app service

runAs ()
{
  sudo su "$NODEJS_USER" -c "$1"
}

pruneApplicationDirectories()
{
  local latestAppSourceFiles=$(runAs "mktemp -q -p ${TMPDIR} --suffix='latestNodeAppSource'")
  local appSourceFiles=$(runAs "mktemp -q -p ${TMPDIR} --suffix='nodeAppSource'")
  local filesToDelete=$(runAs "mktemp -q -p ${TMPDIR} --suffix='nodeAppFilesToDelete'")

  echo >&2 "$COMMAND: Pruning target application directories..."

  if [[ -s "$JENKINS_PRUNE" ]]
  then
    
    runAs "cat $JENKINS_PRUNE" | while read directory
    do
      if [[ -d "$DEPLOY_PROJECT_DIR/$directory" && -d "$LATEST_DEPLOY_PROJECT_DIR/$directory" ]]
      then
        cd "$DEPLOY_PROJECT_DIR"
        runAs "find $directory >> $appSourceFiles 2>&1"
        
        cd "$LATEST_DEPLOY_PROJECT_DIR"
        runAs "find $directory >> $latestAppSourceFiles 2>&1"
      fi
    done

    echo >&2 "$COMMAND: Removing application source files that are not part of latest changesets..."

    if [[ -s "$appSourceFiles" && -s "$latestAppSourceFiles" ]]
    then
      comm -32 \
        <(runAs "sort $appSourceFiles") <(runAs "sort $latestAppSourceFiles") | \
          runAs "tee $filesToDelete"

      echo >&2 "$COMMAND: Removing "$(runAs "wc -l < $filesToDelete")" files..."

      if [[ -s "$filesToDelete" ]]
      then
        cd "$DEPLOY_PROJECT_DIR"
        runAs "cat $filesToDelete | xargs -i rm -vrf {} 2>&1"
      fi
    fi

    echo >&2 "$COMMAND: Removing temporary files..."
    
    runAs "rm -vf $latestAppSourceFiles 2>&1"
    runAs "rm -vf $appSourceFiles 2>&1"
    runAs "rm -vf $filesToDelete 2>&1"
  fi
}

COMMAND=$0

if [[ "$#" != 5 ]]
then
  echo >&2 "$COMMAND: expects 5 arguments ie. LOCAL_PROJECT PM_COMMAND APP_ENTRY_POINT APP_PORT APP_ENVIRONMENT"
fi

LOCAL_PROJECT="$1"
PM_COMMAND="${2:-'start'}"
APP_ENTRY_POINT="${3:-'index.js'}"
APP_PORT="${4:-'5010'}"
APP_ENVIRONMENT="${5:-'staging'}"

: ${NODEJS_HOME='/var/lib/nodejs'}
: ${NODEJS_USER='nodejs'}
: ${APPS_DIR="${NODEJS_HOME}/apps"}
: ${TMPDIR="$NODEJS_HOME/tmp"}
: ${JENKINS_WORKSPACE_DIR='/var/lib/jenkins/workspace'}
: ${PROJECT_DIR="${JENKINS_WORKSPACE_DIR}/${LOCAL_PROJECT}"}
: ${DEPLOY_PROJECT_DIR="${APPS_DIR}/${LOCAL_PROJECT}"}
: ${LATEST_DEPLOY_PROJECT_DIR="${APPS_DIR}/latest_${LOCAL_PROJECT}"}
: ${APP_NAME="${LOCAL_PROJECT}"}
: ${PROCESS_MANAGER='/usr/local/bin/node-pm2'}
: ${PACKAGE_MANAGER='/usr/local/bin/npm'}
: ${JENKINS_BUILD="${DEPLOY_PROJECT_DIR}/jenkins.build"}
: ${JENKINS_PRUNE="${DEPLOY_PROJECT_DIR}/jenkins.prune"}
: ${PAUSE=2}

echo $COMMAND: Checking existence of project directory...

if [ ! -d "$PROJECT_DIR" ]
then
  echo >&2 "$COMMAND: No project found in Jenkins workspace. Build aborted."
  exit 1
fi

echo $COMMAND: Creating application\'s directory if there is none...

if [ ! -d "$DEPLOY_PROJECT_DIR" ]
then
  runAs "mkdir -m 775 -p $DEPLOY_PROJECT_DIR"
fi

echo $COMMAND: Creating directory that holds latest changesets...

if [ ! -d "$LATEST_DEPLOY_PROJECT_DIR" ]
then
  runAs "mkdir -m 775 -p $LATEST_DEPLOY_PROJECT_DIR"
fi

echo $COMMAND: Copying changesets temporarily to application\'s latest deploy location...

runAs "cp -ap ${PROJECT_DIR}/* ${LATEST_DEPLOY_PROJECT_DIR}"

pruneApplicationDirectories

echo $COMMAND: Copying changesets to application\'s deploy location...

runAs "cp -ap ${LATEST_DEPLOY_PROJECT_DIR}/* ${DEPLOY_PROJECT_DIR}"

echo $COMMAND: Removing latest application\'s deploy location...

runAs "rm -rf $LATEST_DEPLOY_PROJECT_DIR 2>&1"

echo $COMMAND: Changing to application\'s deploy location...

cd "$DEPLOY_PROJECT_DIR"

echo $COMMAND: Refreshing and installing application\'s dependencies...

runAs "${PACKAGE_MANAGER} install"

if [[ -s "$JENKINS_BUILD" ]]
then
  echo $COMMAND: Running commands in ${JENKINS_BUILD}...

  runAs "cat $JENKINS_BUILD" | while IFS='\n' read command
  do
    echo $COMMAND: $command
    runAs "$command 2>&1"
    sleep $PAUSE
  done
fi

isRunning=$(runAs "$PROCESS_MANAGER show $APP_NAME > /dev/null 2>&1 && echo \$?")

if [[ "$isRunning" == 0 ]]
then
  echo $COMMAND: Restarting application...
  
  runAs "NODE_ENV=${APP_ENVIRONMENT} PORT=${APP_PORT} $PROCESS_MANAGER restart --update-env $APP_NAME"
else
  echo $COMMAND: Starting application...
  
  runAs "NODE_ENV=${APP_ENVIRONMENT} PORT=${APP_PORT} $PROCESS_MANAGER --name $APP_NAME ${PM_COMMAND} ${APP_ENTRY_POINT}"
fi

runAs "$PROCESS_MANAGER show $APP_NAME"

exit 0

## -- finish
